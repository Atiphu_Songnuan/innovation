<?php
class ApiService
{
    public function loadModel($name)
    {
        $path = 'models/' . $name . '_model.php';
        if (file_exists($path)) {
            require_once 'models/' . $name . '_model.php';
            $modelName = $name . '_Model';
            $this->model = new $modelName();
        }
    }

    public function SearchPerson()
    {
        $this->loadModel("main");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        // echo $request->mainID;
        // $mID = $request->mainID;
        $this->model->SearchPerson($request->registerid);
    }

    public function Registered()
    {
        $this->loadModel("main");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        // echo $request->mainID;
        // $mID = $request->mainID;
        $this->model->Registered($request);
    }

    public function CheckRepeatRegister()
    {
        $this->loadModel("main");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $this->model->CheckRepeatRegister($request->registerid);
    }
}
