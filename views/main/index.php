<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Innovation</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <link href="./public/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css"> -->

    <!-- <link href="./public/css/qrcode/qrcode.min.css" rel="stylesheet">
    <link href="./public/css/qrcode/qrcodestyle.css" rel="stylesheet"> -->

    <!--===============================================================================================-->
      <link rel="icon" type="image/png" href="./public/vendor/login/images/icons/lamp.ico"/>
    <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="./public/vendor/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="./public/vendor/login/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="./public/vendor/login/vendor/animate/animate.css">
    <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="./public/vendor/login/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="./public/vendor/login/css/util.css">
      <link rel="stylesheet" type="text/css" href="./public/vendor/login/css/main.css">
    <!--===============================================================================================-->

    <!-- SweetAlert2 -->
      <link rel="stylesheet" href="./public/vendor/sweetalert2/dist/sweetalert2.min.css">
    <!-- Google Font -->
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> -->
    <link href="https://fonts.googleapis.com/css?family=Lobster|Poppins|Kanit" rel="stylesheet">

    <style>
      /* html, body{
        height: 100%;
        font-family: 'Kanit', cursive;
      }
      label{
        font-family: 'Poppins', sans-serif;
      }
      h5{
        font-family: 'Kanit', serif;
      } */
      .table-responsive {
        min-height: .01%;
        overflow-x: hidden;
      }
      div.dataTables_info , a{
        font-size: 14px;
      }
      div.table-responsive > div.dataTables_wrapper > div.row > div[class^="col-"]:last-child {
        padding-right: 0;
        overflow-x: auto;
      }

      table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td {
        border-bottom-width: 0;
        /* width: 100px; */
        white-space: nowrap;
        vertical-align: middle;
        width: 1%;
      }

      .table>caption+thead>tr:first-child>td, .table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>td, .table>thead:first-child>tr:first-child>th {
        border-top: 0;
        white-space: nowrap;
        width: 1%;
      }

      td{
        white-space:nowrap;
        font-size: 14px;
        /* vertical-align: middle; */
      }
      tr{
        font-family: 'Kanit', serif;
        font-size: 16px;
      }
      label.label-data{
        font-size: 20px;
      }
      .modal{
        /* display: block !important; */
        font-family: 'Kanit', serif;
      }
     
      .btn-scan{
        justify-content: center;
        align-items: center;
        padding: 0 20px;
        width: 100%;
        height: 60px;
        background-color: #3b5998;
        border-radius: 10px;

        font-family: 'Kanit', serif;
        font-size: 18px;
        color: #fff;
        line-height: 1.2;

        -webkit-transition: all 0.4s;
        -o-transition: all 0.4s;
        -moz-transition: all 0.4s;
        transition: all 0.4s;
        position: relative;
        z-index: 1;
      }
      .btn-scan::before{
        content: "";
        display: block;
        position: absolute;
        z-index: -1;
        width: 100%;
        height: 100%;
        border-radius: 10px;
        top: 0;
        left: 0;
        background: #a64bf4;
        background: -webkit-linear-gradient(45deg, #00dbde, #fc00ff);
        background: -o-linear-gradient(45deg, #00dbde, #fc00ff);
        background: -moz-linear-gradient(45deg, #00dbde, #fc00ff);
        background: linear-gradient(45deg, #00dbde, #fc00ff);
        opacity: 0;
        -webkit-transition: all 0.4s;
        -o-transition: all 0.4s;
        -moz-transition: all 0.4s;
        transition: all 0.4s;
      }

      .btn-scan:hover:before {
        opacity: 1;
      }

      .swal2-popup {
        font-family: 'Kanit', serif;
        font-size: 0.7rem !important;
      }

    </style>
  </head>
  <body>
    <!-- <section class="content-header">
    </section>

    <section class="content">
      <div class="container-fluid text-center mt-4">
        <div class="row">
          <div class="col-md-12 mb-4">
            <h3 class="text-info shadow-lg">Registration</h3>
          </div>

          <div class="col-md-12">
            <label class="btn btn-primary" style="box-shadow: 0 8px 8px 0 rgba(0,0,0,0.2), 0 8px 8px 0 rgba(0,0,0,0.19); padding: 14px 40px;">
              Scan QR Code
              <input type=file accept="image/*" capture=environment onchange="OpenQRCamera(this);" id="btnuploadqrcode" hidden>
              <span><i class="fa fa-qrcode"></i></span>
            </label>
          </div>
        </div>
      </div>
    </section> -->

    <div class="limiter">
      <div class="container-login100" style="background-image: url('./public/vendor/login/images/green.png');">
        <div class="wrap-login100 p-l-110 p-r-110 p-t-30 p-b-30 text-center">
          <img src="./public/img/lamp.svg" width="80" height="80" alt="logo">
          <br>
          <img src="./public/img/psu_logo.png" width="55" height="40" alt="logo">              
          <span class="login100-form-title p-t-5 m-b-10" style="font-family: 'Kanit', serif; font-size:26px">
          ประชุมวิชาการ<br>นวัตกรรมทางการแพทย์<br><small style="font-size: 16px">คณะแพทยศาสตร์ ม.อ. ครั้งที่ 1</small>
          </span>
          <span class="login100-form-title p-t-15 m-b-10" style="font-family: 'Kanit', serif; font-size:26px">
            <small>ลงทะเบียนเข้าร่วมงาน</small>
          </span>

          <!-- <select class="form-control" id="camera-select" hidden></select>
          <div class="form-group">
            <input id="image-url" type="text" class="form-control" placeholder="Image url" hidden>
            <button title="Decode Image" class="btn-scan" id="decode-img" type="button" data-toggle="tooltip" style="box-shadow: 0 8px 8px 0 rgba(0,0,0,0.2), 0 8px 8px 0 rgba(0,0,0,0.19); border-radius: 10px; text-align: center;">Scan QR Code <span class="fa fa-qrcode"></span></button>
          </div> -->

          <label class="btn-scan m-b-10 text-center" style="box-shadow: 0 8px 8px 0 rgba(0,0,0,0.2), 0 8px 8px 0 rgba(0,0,0,0.19); padding-top: 20px;">
            Scan QR Code
            <input type=file accept="image/*" capture=environment onchange="OpenQRCamera(this);" id="btnuploadqrcode" hidden>
            <span><i class="fa fa-qrcode"></i></span>
          </label>

          <div class="login100-form-title m-t-30 text-center" >
            <span class="text-danger" style="font-family: 'Kanit', serif; font-size:22px;">
              หรือ
            </span>
          </div>


          <div class="login100-form-title p-b-10 m-t-10 text-center" >
            <span style="font-family: 'Kanit', serif; font-size:22px;">
              รหัสลงทะเบียน
            </span>
          </div>
          <div class="wrap-input100 validate-input m-b-30" data-validate = "Register ID is required">
            <input class="input100" type="text" id="inputregisterid" style="font-family: 'Kanit', serif; box-shadow: 0 8px 8px 0 rgba(0,0,0,0.2), 0 8px 8px 0 rgba(0,0,0,0.19); border-radius: 10px; text-align: center;">
            <span class="focus-input100"></span>
          </div>

          <div class="container-login100-form-btn m-b-30">
            <button class="login100-form-btn" id="btnsearch" onclick="ConvertToMD5()" style="background-color:#28a745; font-family: 'Kanit', serif; box-shadow: 0 8px 8px 0 rgba(0,0,0,0.2), 0 8px 8px 0 rgba(0,0,0,0.19); border-radius: 10px; text-align: center;">
              ค้นหา
            </button>
          </div>

          <div class="container-login100-form-btn">
            <button class="login100-form-btn" id="btnviewregister" onclick="ViewRegisterTable()" style="background-color:#343a40; font-family: 'Kanit', serif; box-shadow: 0 8px 8px 0 rgba(0,0,0,0.2), 0 8px 8px 0 rgba(0,0,0,0.19); border-radius: 10px; text-align: center;">
              รายชื่อผู้ลงทะเบียน
            </button>
          </div>

          <div class="w-full text-center p-t-10">
            <span class="txt2">
              <div class="panel panel-info">
              <div class="navbar-form navbar-right">
                <div class="panel-body text-center">
                  <div class="col-md-6" hidden>
                    <div class="well" style="position: relative;display: inline-block;">
                      <canvas width="320" height="240" id="webcodecam-canvas"></canvas>
                      <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                      <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                      <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                      <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div id="result">
                      <div class="well" style="overflow: hidden;" hidden>
                        <img width="320" height="240" id="scanned-img" src="">
                      </div>
                      <div class="caption">
                        <p id="scanned-QR"></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </span>
          </div>
        </div>
      </div>
    </div>

    <!-- <div class="modal fade text-center" id="personal-modal">
      <div class="modal-dialog modal-dialog-centered" style="width: 80%;">
        <div class="modal-content">
          <div class="modal-header text-center">
            <h4 class="col-12 modal-title">ข้อมูลผู้ลงทะเบียน</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div> -->
    <div class="modal fade" id="personal-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header text-center">
            <h5 class="modal-title w-100" id="exampleModalLabel"><i class="fas fa-user-circle"></i> ข้อมูลผู้ลงทะเบียน</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12 m-b-15" hidden>
                <label>รหัสลงทะเบียน</label>
                <label class="label-data text-info" id="registerid"></label>
              </div>

              <div class="col-md-12 m-b-15 p-l-15">
                <label style="font-weight:bold;"><i class="fas fa-address-card"></i> ชื่อ-นามสกุล</label>
                <label class="label-data text-info p-l-23" id="namelabel"></label>
              </div>

              <div class="col-md-12 m-b-15 p-l-15">
                <label style="font-weight:bold;"><i class="fas fa-envelope"></i> email</label>
                <label class="label-data text-info p-l-23" id="emaillabel"></label>
              </div>

              <div class="col-md-12 m-b-15 p-l-15">
                <label style="font-weight:bold;"><i class="fas fa-briefcase"></i> ตำแหน่งงาน/อาชีพ</label>
                <label class="label-data text-info p-l-23" id="positionlabel"></label>
              </div>

              <div class="col-md-12 m-b-15 p-l-15">
                <label style="font-weight:bold;"><i class="fas fa-building"></i> หน่วยงาน</label>
                <label class="label-data text-info p-l-23" id="departlabel"></label>
              </div>

              <div class="col-md-12 m-b-15 p-l-15">
                <label style="font-weight:bold;"><i class="fas fa-map-marker-alt"></i> สถานที่ทำงาน</label>
                <label class="label-data text-info p-l-23" id="departaddresslabel"></label>
              </div>

              <div class="col-md-12 m-b-15 p-l-15">
                <label style="font-weight:bold;"><i class="fas fa-mobile-alt"></i> เบอร์โทรติดต่อ</label>
                <label class="label-data text-info p-l-23" id="phonelabel"></label>
              </div>

              <div class="col-md-12 p-l-15">
                <label style="font-weight:bold;"><i class="fas fa-utensils"></i> ประเภทอาหาร</label>
                <label class="label-data text-info p-l-23" id="foodtypelabel"></label>
              </div>
            </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
              <button type="button" onclick="RegisterConfirm()" id="btnconfirm" class="btn btn-success">ลงทะเบียนเข้างาน</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="register-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header text-center">
            <h5 class="modal-title w-100" id="exampleModalLabel"><i class="fas fa-user-circle"></i> รายชื่อผู้ลงทะเบียน</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12 m-b-15">
                <div class="table-responsive">
                  <table class="table table-bordered" id="registertable" width="100%" cellspacing="0">
                    <thead>
                      <tr class="text-gray-800">
                        <th>ชื่อ-นามสกุล</th>
                        <th>ตำแหน่งงาน</th>
                        <th>เบอร์โทรติดต่อ</th>
                        <th>เวลาที่ลงทะเบียน</th>

                      </tr>
                    </thead>
                    <tbody>
                    <?php
                        $personregisterJsonDecode = json_decode($this->PersonRegisterData, true);
                        for ($i = 0; $i < count($personregisterJsonDecode); $i++) {
                            echo '<tr class="text-gray-800">';

                            echo '<td class="text-success">';
                            if ($personregisterJsonDecode[$i]['NAME'] != "") {
                                echo $personregisterJsonDecode[$i]['NAME_TITLE']. $personregisterJsonDecode[$i]['NAME']." ".$personregisterJsonDecode[$i]['SURNAME'];
                            } else {
                                echo "-";
                            }
                            echo '</td>';

                            echo '<td>';
                            echo '<span class="badge badge-info" style="font-size:14px;">';
                            if ($personregisterJsonDecode[$i]['POSITION'] != "") {
                                echo $personregisterJsonDecode[$i]['POSITION'];
                            } else {
                                echo "-";
                            }
                            echo '</span>';
                            echo '</td>';

                            echo '<td align="center">';
                            echo '<span class="badge badge-success" style="font-size:14px;">';
                            if ($personregisterJsonDecode[$i]['PHONE_NUMBER'] != "") {
                                echo $personregisterJsonDecode[$i]['PHONE_NUMBER'];
                            } else {
                                echo "-";
                            }
                            echo '</span>';
                            echo '</td>';

                            echo '<td align="center">';
                            echo '<span class="badge badge-danger" style="font-size:14px;">';
                            if ($personregisterJsonDecode[$i]['REGISTER_TIME'] != "") {
                                echo $personregisterJsonDecode[$i]['REGISTER_TIME'];
                            } else {
                                echo "-";
                            }
                            echo '</span>';
                            echo '</td>';

                            echo '</tr>';
                        }
                      ?>
                      <!-- <tr class="text-gray-800">
                        <td>วันที่</td>
                        <td>วันที่</td>
                        <td>วันที่</td>
                        <td>วันที่</td>
                      </tr> -->
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
          </div>
        </div>
      </div>
    </div>

    <script src="./public/vendor/jquery/jquery.min.js"></script>

    <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <!--===============================================================================================-->
      <script src="./public/vendor/login/vendor/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
      <script src="./public/vendor/login/vendor/select2/select2.min.js"></script>
    <!--===============================================================================================-->
      <script src="./public/vendor/login/vendor/countdowntime/countdowntime.js"></script>
    <!--===============================================================================================-->
      <script src="./public/vendor/login/js/main.js"></script>

      <script src="./public/js/md5.min.js"></script>

      <script src="./public/js/qr_packed.js"></script>

    <!-- Datatable -->
      <script src="./public/vendor/datatables/jquery.dataTables.js"></script>
      <script src="./public/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Demo scripts for this page-->
      <script src="./public/js/datatables-demo.js"></script>

      <!-- <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script> -->


    <!-- SweetAlert2 -->
      <script src="./public/vendor/sweetalert2/dist/sweetalert2.min.js"></script>

    <!-- QR Code Scanner -->
      <script src="./public/js/qrcode/filereader.js"></script>
      <script src="./public/js/qrcode/qrcodelib.js"></script>
      <script src="./public/js/qrcode/webcodecamjs.js"></script>
      <script src="./public/js/qrcode/main.js"></script>

    <script>
      var jsondata = "";
      var registerid = "";
      function OpenQRCamera(node) {
        registerid = "";
        var reader = new FileReader();
        reader.onload = function() {
            node.value = "";
            qrcode.callback = function(res) {
                // var parameters = res.split("?")[1].split('&');
                // // var registerid = [];
                // parameters.forEach(function(element){
                //     registerid.push(element.split("=")[1]);
                // });
                registerid = res;
                // console.log(res);
                if(res instanceof Error) {
                    // alert("No QR code found. Please make sure the QR code is within the camera's frame and try again.");
                    // alert("ไม่พบ QR Code. กรุณา Scan QR Code ใหม่อีกครั้ง.");
                    $('#inputregisterid').val('');
                    Swal.fire({
                        title: 'ไม่พบ QR Code. กรุณา Scan QR Code ใหม่อีกครั้ง.',
                        type: 'error',
                        timer: 3000,
                        showConfirmButton: true,
                    })
                } else {
                    // node.parentNode.previousElementSibling.value = res;
                    // jsondata = {"registerid": registerid};
                    SearchPerson(registerid);
                    // console.log(jsondata);

                    // var data = jQuery.parseJSON(jsondata['registerid']);
                    // console.log(data["type"]);

                    // $cryptKey  = 'qJB0rGtIn5UB1xG03efyCp';
                    // $qDecoded      = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), base64_decode( $q ), MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ), "\0");
                }
            };
            qrcode.decode(reader.result);
        };
        reader.readAsDataURL(node.files[0]);
      }

      function ConvertToMD5(inputdata){
        if ($('#inputregisterid').val() != "") {
          var registeridmd5 = md5($('#inputregisterid').val());
          SearchPerson(registeridmd5);
        } else{
          Swal.fire({
            position: 'center',
            type: 'warning',
            title: 'กรุณากรอกรหัสผู้ลงทะเบียนให้ถูกต้อง',
            showConfirmButton: false,
            timer: 1500
          });
        }
      }

      function SearchPerson(registerid){
        jsondata = {"registerid": registerid};
        // console.log(jsondata);
        $.ajax({
          type:"POST",
          url: "../innovation/ApiService/SearchPerson",
          header:{
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: JSON.stringify(jsondata),
          contentType: "application/json",
          dataType: "json",
          success: function(response){
            // console.log(response);
            if (response.length != 0) {
              registeridjson = {"registerid": response[0]['REGISTER_ID']};
              $.ajax({
                type:"POST",
                url:"../innovation/ApiService/CheckRepeatRegister",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: JSON.stringify(registeridjson),
                contentType: "application/json",
                dataType: "json",
                success: function(response_duplicate) {
                  if (response_duplicate.length == 0) {
                    $('#personal-modal').modal();

                    $('#registerid').text(response[0]['REGISTER_ID']);

                    $('#namelabel').text(response[0]['NAME_TITLE'] + response[0]['NAME'] + " " + response[0]['SURNAME']);
                    if (response[0]['EMAIL'] != null) {
                      $('#emaillabel').text(response[0]['EMAIL']);
                      $('#emaillabel').attr("class", "text-info p-l-23");
                    } else{
                      $('#emaillabel').text("-");
                      $('#emaillabel').attr("class", "text-danger p-l-23");
                    }
                    $('#positionlabel').text(response[0]['POSITION']);
                    $('#departlabel').text(response[0]['DEPART_NAME']);

                    if (response[0]['DEPART_ADDRESS'] != null) {
                      $('#departaddresslabel').text(response[0]['DEPART_ADDRESS']);
                      $('#departaddresslabel').attr("class", "text-info p-l-23");
                    } else{
                      $('#departaddresslabel').text("-");
                      $('#departaddresslabel').attr("class", "text-danger p-l-23");
                    }

                    if (response[0]['PHONE_NUMBER'] != null) {
                      $('#phonelabel').text(response[0]['PHONE_NUMBER']);
                      $('#phonelabel').attr("class", "text-info p-l-23");
                    } else{
                      $('#phonelabel').text("-");
                      $('#phonelabel').attr("class", "text-danger p-l-23");
                    }

                    if (response[0]['FOOD_TYPE'] != null) {
                      $('#foodtypelabel').text(response[0]['FOOD_TYPE']);
                      $('#foodtypelabel').attr("class", "text-info p-l-23");
                    } else{
                      $('#foodtypelabel').text("-");
                      $('#foodtypelabel').attr("class", "text-danger p-l-23");
                    }
                  } else{
                    Swal.fire({
                      position: 'center',
                      type: 'warning',
                      title: 'คุณได้ลงทะเบียนเข้างานแล้ว',
                      // showConfirmButton: false,
                      // timer: 1500
                    });
                  }
                },
                error: function(response_duplicate){
                  console.log(response_duplicate);
                }
              });
            } else{
              Swal.fire({
                  title: 'ไม่พบข้อมูลการลงทะเบียน, กรุณา Scan QR Code ใหม่อีกครั้ง.',
                  type: 'error',
                  // timer: 3000,
                  showConfirmButton: true,
              })
            }
          },
          error: function(response){
            console.log(response);
          }
        });
      }

      var table;
      $(function(){
        var data = <?php echo $this->PersonRegisterData;?>;
        table = $('#registertable').dataTable({
          // "order": [[ 3, "desc" ]],
          "searching": false,
          "ordering": false,
          "aLengthMenu": [5],
          "iDisplayLength": 5,
          // "ajax": data
          // "paging": false,
          // "bFilter": false
          // "bInfo": false,
          // "bAutoWidth": false,
          // "bLengthChange": false
          // "columnDefs": [
          //   {
          //     "targets": [ 0 ],
          //     "visible": false, //ซ่อน column ตาม targets ที่กำหนดไว้
          //     "searchable": false
          //   }
          // ]
        });

        // setInterval( function () {
        //   table.ajax.reload(); // user paging is not reset on reload
        // }, 3000 );
      });

      // (function(undefined) {
        //     "use strict";
        //     function Q(el) {
        //       if (typeof el === "string") {
        //         var els = document.querySelectorAll(el);
        //         return typeof els === "undefined" ? undefined : els.length > 1 ? els : els[0];
        //       }
        //       return el;
        //     }
        //     var txt = "innerText" in HTMLElement.prototype ? "innerText" : "textContent";
        //     var scannerLaser = Q(".scanner-laser"),
        //       imageUrl = new Q("#image-url"),
        //       // play = Q("#play"),
        //       scannedImg = Q("#scanned-img"),
        //       scannedQR = Q("#scanned-QR"),
        //       grabImg = Q("#grab-img"),
        //       decodeLocal = Q("#decode-img");

        //     var args = {
        //       autoBrightnessValue: 100,
        //       resultFunction: function(res) {
        //         [].forEach.call(scannerLaser, function(el) {
        //           fadeOut(el, 0.5);
        //           setTimeout(function() {
        //             fadeIn(el, 0.5);
        //           }, 300);
        //         });
        //         // scannedImg.src = res.imgData;
        //         // scannedQR[txt] = res.format + ": " + res.code;

        //         var registerid = res.code;
        //         // console.log(registerid);
        //         SearchPerson(registerid);
        //         // Swal.fire({
        //         //   position: 'center',
        //         //   type: 'warning',
        //         //   text: registerid,
        //         //   showConfirmButton: false,
        //         //   timer: 1500
        //         // });
        //       },
        //       getDevicesError: function(error) {
        //         var p, message = "Error detected with the following parameters:\n";
        //         for (p in error) {
        //           message += p + ": " + error[p] + "\n";
        //         }
        //         alert(message);
        //       },
        //       getUserMediaError: function(error) {
        //           var p, message = "Error detected with the following parameters:\n";
        //           for (p in error) {
        //             message += p + ": " + error[p] + "\n";
        //           }
        //           alert(message);
        //       },
        //       cameraError: function(error) {
        //           var p, message = "Error detected with the following parameters:\n";
        //           if (error.name == "NotSupportedError") {
        //             var ans = confirm("Your browser does not support getUserMedia via HTTP!\n(see: https:goo.gl/Y0ZkNV).\n You want to see github demo page in a new window?");
        //             if (ans) {
        //               window.open("https://andrastoth.github.io/webcodecamjs/");
        //             }
        //           } else {
        //             for (p in error) {
        //               message += p + ": " + error[p] + "\n";
        //             }
        //             alert(message);
        //           }
        //       },
        //       cameraSuccess: function() {
        //         grabImg.classList.remove("disabled");
        //       },
        //       cameraError: function(){
        //         console.log("Cannot Scan");
        //       }
        //     };
        //     var decoder = new WebCodeCamJS("#webcodecam-canvas").buildSelectMenu("#camera-select", "environment|back").init(args);
        //     decodeLocal.addEventListener("click", function() {
        //       Page.decodeLocalImage();
        //     }, false);

        //     Page.decodeLocalImage = function() {
        //       if (decoder.isInitialized()) {
        //         decoder.decodeLocalImage(imageUrl.value);
        //       }
        //       imageUrl.value = null;
        //     };
        //     var getZomm = setInterval(function() {
        //       var a;
        //       try {
        //         a = decoder.getOptimalZoom();
        //       } catch (e) {
        //         a = 0;
        //       }
        //       if (!!a && a !== 0) {
        //         Page.changeZoom(a);
        //         clearInterval(getZomm);
        //       }
        //     }, 500);

        //     function fadeOut(el, v) {
        //       el.style.opacity = 1;
        //       (function fade() {
        //           if ((el.style.opacity -= 0.1) < v) {
        //             el.style.display = "none";
        //             el.classList.add("is-hidden");
        //           } else {
        //             requestAnimationFrame(fade);
        //           }
        //       })();
        //     }

        //     function fadeIn(el, v, display) {
        //       if (el.classList.contains("is-hidden")) {
        //           el.classList.remove("is-hidden");
        //       }
        //       el.style.opacity = 0;
        //       el.style.display = display || "block";
        //       (function fade() {
        //           var val = parseFloat(el.style.opacity);
        //           if (!((val += 0.1) > v)) {
        //               el.style.opacity = val;
        //               requestAnimationFrame(fade);
        //           }
        //       })();
        //     }
      // }).call(window.Page = window.Page || {});

      function RegisterConfirm(){
        var jsondata = {"registerid": $('#registerid').text()}
                        // "name": $('#namelabel').text(),
                        // "email": $('#emaillabel').text(),
                        // "position": $('#positionlabel').text(),
                        // "departname": $('#departlabel').text(),
                        // "foodtype": $('#foodtypelabel').text()};
        $.ajax({
          type:"POST",
          url: "../innovation/ApiService/Registered",
          header:{
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: JSON.stringify(jsondata),
          contentType: "application/json",
          dataType: "json",
          success: function(response){
            // console.log(response);
            Swal.fire({
              position: 'center',
              type: 'success',
              title: 'ลงทะเบียนเข้างานสำเร็จ',
              showConfirmButton: false,
              timer: 1500,
              onClose: () =>{
                  window.location.href='../innovation/main';
              }
            });
          },
          error: function(response){
            Swal.fire({
              position: 'center',
              type: 'warning',
              title: 'กรุณากรอกรหัสผู้ลงทะเบียนให้ถูกต้อง',
              showConfirmButton: false,
              timer: 1500
            });
          }
        });
      }

      function ViewRegisterTable(){
        $('#register-modal').modal();
      }
    </script>
  </body>
</html>