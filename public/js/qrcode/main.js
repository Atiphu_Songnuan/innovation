/*!
 * WebCodeCamJS 2.1.0 javascript Bar code and QR code decoder 
 * Author: Tóth András
 * Web: http://atandrastoth.co.uk
 * email: atandrastoth@gmail.com
 * Licensed under the MIT license
 */
// (function(undefined) {
//     "use strict";

//     function Q(el) {
//         if (typeof el === "string") {
//             var els = document.querySelectorAll(el);
//             return typeof els === "undefined" ? undefined : els.length > 1 ? els : els[0];
//         }
//         return el;
//     }
//     var txt = "innerText" in HTMLElement.prototype ? "innerText" : "textContent";
//     var scannerLaser = Q(".scanner-laser"),
//         imageUrl = new Q("#image-url"),
//         // play = Q("#play"),
//         scannedImg = Q("#scanned-img"),
//         scannedQR = Q("#scanned-QR"),
//         grabImg = Q("#grab-img"),
//         decodeLocal = Q("#decode-img");
    
//     var args = {
//         autoBrightnessValue: 100,
//         resultFunction: function(res) {
//             [].forEach.call(scannerLaser, function(el) {
//                 fadeOut(el, 0.5);
//                 setTimeout(function() {
//                     fadeIn(el, 0.5);
//                 }, 300);
//             });
//             scannedImg.src = res.imgData;
//             scannedQR[txt] = res.format + ": " + res.code;
//         },
//         getDevicesError: function(error) {
//             var p, message = "Error detected with the following parameters:\n";
//             for (p in error) {
//                 message += p + ": " + error[p] + "\n";
//             }
//             alert(message);
//         },
//         getUserMediaError: function(error) {
//             var p, message = "Error detected with the following parameters:\n";
//             for (p in error) {
//                 message += p + ": " + error[p] + "\n";
//             }
//             alert(message);
//         },
//         cameraError: function(error) {
//             var p, message = "Error detected with the following parameters:\n";
//             if (error.name == "NotSupportedError") {
//                 var ans = confirm("Your browser does not support getUserMedia via HTTP!\n(see: https:goo.gl/Y0ZkNV).\n You want to see github demo page in a new window?");
//                 if (ans) {
//                     window.open("https://andrastoth.github.io/webcodecamjs/");
//                 }
//             } else {
//                 for (p in error) {
//                     message += p + ": " + error[p] + "\n";
//                 }
//                 alert(message);
//             }
//         },
//         cameraSuccess: function() {
//             console.log("Success");
//             grabImg.classList.remove("disabled");
//         }
//     };
//     var decoder = new WebCodeCamJS("#webcodecam-canvas").buildSelectMenu("#camera-select", "environment|back").init(args);
//     decodeLocal.addEventListener("click", function() {
//         Page.decodeLocalImage();
//     }, false);

//     Page.decodeLocalImage = function() {
//         if (decoder.isInitialized()) {
//             decoder.decodeLocalImage(imageUrl.value);
//         }
//         imageUrl.value = null;
//     };
//     var getZomm = setInterval(function() {
//         var a;
//         try {
//             a = decoder.getOptimalZoom();
//         } catch (e) {
//             a = 0;
//         }
//         if (!!a && a !== 0) {
//             Page.changeZoom(a);
//             clearInterval(getZomm);
//         }
//     }, 500);

//     function fadeOut(el, v) {
//         el.style.opacity = 1;
//         (function fade() {
//             if ((el.style.opacity -= 0.1) < v) {
//                 el.style.display = "none";
//                 el.classList.add("is-hidden");
//             } else {
//                 requestAnimationFrame(fade);
//             }
//         })();
//     }

//     function fadeIn(el, v, display) {
//         if (el.classList.contains("is-hidden")) {
//             el.classList.remove("is-hidden");
//         }
//         el.style.opacity = 0;
//         el.style.display = display || "block";
//         (function fade() {
//             var val = parseFloat(el.style.opacity);
//             if (!((val += 0.1) > v)) {
//                 el.style.opacity = val;
//                 requestAnimationFrame(fade);
//             }
//         })();
//     }
// }).call(window.Page = window.Page || {});