<?php
class Main_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function SearchPerson($registerid)
    {
        $sql = "SELECT REGISTER_ID, NAME_TITLE, NAME, SURNAME, EMAIL, POSITION, DEPART_NAME, DEPART_ADDRESS, PHONE_NUMBER, FOOD_TYPE
                FROM persons
                WHERE GENERATE_ID = '$registerid'";
        $sth = $this->db->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll();
        $jsonData = json_encode($data);
        echo $jsonData;
    }

    public function Registered($registerdata)
    {
        if (isset($registerdata)) {

            // $email = $registerdata->email;
            // if ($email == '-') {
            //     $email = null;
            // }

            // $foodtype = $registerdata->foodtype;
            // if ($foodtype == '-') {
            //     $foodtype = null;
            // }

            $data = [
                'registerid' => $registerdata->registerid,
                // 'name' => $registerdata->name,
                // 'email' => $email,
                // 'position' => $registerdata->position,
                // 'departname' => $registerdata->departname,
                // 'foodtype' => $foodtype,
                'registerstatus' => 'Y',
            ];

            // $sql = "INSERT INTO  register(REGISTER_ID, NAME, EMAIL, POSITION, DEPART_NAME, FOOD_TYPE, REGISTER_STATUS, REGISTER_DATE, REGISTER_TIME)
            //         VALUES  (:registerid, :name, :email, :position, :departname, :foodtype, :registerstatus, NOW(), NOW())";

            $sql = "INSERT INTO  register(REGISTER_ID, REGISTER_STATUS, REGISTER_DATE, REGISTER_TIME)
                    VALUES  (:registerid, :registerstatus, NOW(), NOW())";

            $sth = $this->db->prepare($sql);
            $sth->execute($data);

            echo true;
        } else {
            echo false;
        }
    }

    public function CheckRepeatRegister($registerid)
    {
        $sql = "SELECT REGISTER_ID, NAME_TITLE, NAME, SURNAME, EMAIL, REGISTER_DATE FROM viewpersonsregister WHERE REGISTER_ID = '$registerid' AND DATE(REGISTER_DATE) = CURDATE()";
        $sth = $this->db->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll();
        $jsonData = json_encode($data);
        echo $jsonData;
    }

    public function GetPersonRegisterData()
    {
        // echo json_encode($perid);
        // $encodePassword = md5($password);
        $sql = 'SELECT NAME_TITLE, NAME, SURNAME, POSITION, PHONE_NUMBER, REGISTER_DATE, REGISTER_TIME FROM viewpersonsregister WHERE DATE(REGISTER_DATE) = CURDATE() ORDER BY REGISTER_TIME DESC';
        $sth = $this->db->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll();
        $jsonData = json_encode($data);
        return $jsonData;
    }
}
