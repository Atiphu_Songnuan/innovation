<?php
class Main extends Controller
{
    public function __construct()
    {
        parent::__construct('main');
        $this->views->PersonRegisterData = $this->model->GetPersonRegisterData();
    }

    public function index()
    {
        $this->views->render('main/index');
    }

}
